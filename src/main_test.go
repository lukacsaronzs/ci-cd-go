package main

import (
	"testing"
)

//TestSquareAndCube tests TestSquareAndCube
func TestSquareAndCube(t *testing.T) {
	got1, got2 := SquareAndCube(2)
	if got1 != 4 || got2 != 8 {
		t.Errorf("SquareAndCube(2) = %d, %d; want 4, 8", got1, got2)
	}
}
