package main

import (
	"fmt"
)

//SquareAndCube returns square and cube
func SquareAndCube(a int) (int, int) {
	return a * a, a * a * a
}

func main() {
	fmt.Println("Hello world!")
}
